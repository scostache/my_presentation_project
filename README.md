# My Presentation Project


# PROIECT DE ABSOLVIRE

## SOFTWARE DEVELOPMENT ACADEMY

## COSTACHE SORIN

## Grupa : TesterremoteRO


**PARTEA TEORETICA**

```
 Requirements (Cerintele de business) - Reprezinta asteptarile, cerintele si specificatiile detaliate ale clientului legate de functionalitatea
produsului ce urmeaza sa fie lansat. Include teste functionale, si de asemenea, atribute non-functionale, cum ar fi performanta, fiabilitatea,
domeniul sau capacitatea de utilizare.
```
```
 Test Condition (Conditii de testare) – Reprezinta un aspect testabil al unui element sau al unui eveniment, al unei componente sau
sistem, care poate fi verificat de unul sau mai multe test case-uri, de ex. functie, tranzactie, caracteristica, atribut de calitate sau element de
structura.
```
```
 Test Case (Cazuri de testare) - Este un set de date de intrare, conditii preliminare, rezultate asteptate si condiție de iesire ce trebuie
executate pentru a verifica o caracteristica, o functionalitate sau conformitatea cu o anumita cerinta a produsului testat. Prin compararea
rezultatului asteptat cu rezultatul obtinut verificam daca produsul testat functioneaza conform cerintelor.
```
```
 Test Plan sau Test Suite (Planul de testare) - are scopul de a comunica membrilor echipei abordarea de testare. Include obiectivele,
domeniul de aplicare, metodele, programul, riscurile și abordarea. Acest document va identifica în mod clar care vor fi livrabilele de testare și
ceea ce este considerat in si in afara domeniului de aplicare. Test Plan-ul poate fi scris ca un întreg (pentru fiecare nivel de testare) sau poate
fi pregatit separat pentru fiecare nivel de test individual, totul depinzand de metodologia proiectului.
```
```
 Un test case poate avea urmatoarele statusuri : Passed, Blocked, Retest, Failed, Untested.
```
```
 Statusurile utilizate pentru defecte, pe parcursul ciclului lor de viață sunt: ​​New (nou), Assigned (alocat), Open (deschis), Fixed (fixat),
Retest (retestare), Verified (verificat), Reopen (redeschis), Defered (amanat), Closed (inchis), Rejected (respins), Duplicate (duplicat), Not a bug
(nu este un bug - prin proiectare).
```
```
 Diferenta intre priority (prioritate) si severity (severitate) - Priority determină gradul de urgenta pentru repararea defectului, in functie de
cat este de critic (daca blocheaza aplicatia sau nu are nici un efect asupra aplicației) iar Severity determina impactul defectului asupra
dezvoltării sau funcționării aplicatiei sau sistemului.
```

**PARTEA TEORETICA**

```
 Raportul de testare - este un document care ne informează despre stadiul testelor. El ne permite sa urmarim progresul
testelor, astfel incat sa putem reactiona in cazul unor evenimente neasteptate sau esecuri critice. Cu ajutorul acestui document
partile interesate de produs pot vedea si evalua atat procesul de testare cat si calitatea acestuia.
Conform standardului distingem două tipuri de rapoarte:
Test status report (status raport testare) - arata starea curenta a testelor si se efectueaza la intervale regulate.
Test completion report (raport finalizare testare) - rezuma intregul proces de testare pe baza caruia partile interesate de
produs pot vedea si evalua procesul de testare si rezultatele obtinute si pe baza caruia se poate decide lansarea sau nu a
produsului final.
```
```
 Retesting (retestare) si Regression testing (testarea de regresie).
Retesting (retestarea) verifica partea modificata a programului acolo unde a fost gasit si remediat un defect.
Regression testing (testul de regresie) retesteaza toate modulele sau partile din sistem pentru a ne asigura ca in urma
remedierii defectelor nu au fost afectate alte segmente sau functionalitati ale produsului.
```
```
 Functional testing (Teste functionale) si non-functional testing (Teste non-functionale).
Functional testing (Testele functionale) verifica daca produsul testat functioneaza conform cerintelor clientului. NU testeaza
codul din spate ci doar interfata cu utilizatorul.
Non-functional testing (Testele non-functionale) verifica cat de bine se comporta produsul testat, daca e suficient de rapid, de
intuitiv, ce se intampla daca il supraincarcam, cum functioneaza si pe alte medii, etc.
```

**PARTEA TEORETICA**

```
 Etapele procesului de testare sunt :
```
- _Test Planning (planificarea testelor)._ Este etapa in care se planifica desfasurarea procesului de testare, se defineste
abordarea pentru atingerea obiectivelor de testare (de ex. prin alegerea tehnicilor de testare adecvate, prin definirea obiectivelor
testarii) si se formulează un program de testare (pentru a respecta termenul limită).
- _Test Monitoring and Control (monitorizarea și controlul testelor)._ Se bazează pe compararea reală a planului cu progresul
actual, se stabilește dacă este necesară o extindere a domeniului de aplicare a testului si se informeaza părțile interesate despre
progresul în implementarea sarcinilor.
- _Test Analysis (analiza de testare)._ Este efectuată pentru a defini conditiile de testare (ce ar trebui testat ?). Se analizeaza
baza de testare, sunt identificate conditiile de testare se analizeaza informațiile de proiectare disponibile, se efectuează o evaluare
a testabilității pentru a identifica rapid defectele care pot cauza probleme de testare.
- _- Test Design (proiectarea testarii)._ Raspunde la întrebarea „cum se testează?”. Este etapa in care se creeaza test case-urile si
se definesc prioritatile acestora.
- - _Test Implementation (implementarea testului)._ Implementarea testelor raspunde la intrebarea: „Avem tot ce este necesar
pentru a rula testele?”. Este etapa in care sunt dezvoltate proceduri de testare si sunt definite prioritatile si scripturile potentiale ale
acestora pentru testarea automata. Se construieste si se verifica un mediu de testare, inclusiv servicii, virtualizare, simulatoare si
alta infrastructura necesara.
- _- Test Execution (executarea testului)._ Este etapa in care se executa test case-urile, sunt comparate rezultatele reale și cele
asteptate si sunt semnalate defectele.
- _- Test Completion (finalizarea testului)._ Este etapa in care se verifică dacă toate defectele sunt inchise, se genereaza un raport
final de testare, se trimit rapoartele catre client, se arhiveaza mediul, datele de testare si infrastructura de testare pentru a putea fi
reutilizata ulterior.


**PARTEA TEORETICA**

TEHNICI DE TESTARE

```
TESTAREA STATICA
-Se efectueaza inainte de a fi scris
si executat codul software-ului.
-Cel mai important instrument al
acestei tehnici este review-ul
```
```
TESTAREA DINAMICA
-Implica rularea produsului software
```
```
Black-Box testing – implica testarea unui produs fara a cunoaste
codul intern de functionare.
```
- equivalence partitioning
- boundary values analysis
- state transition testing
- decision table
- use case testing

```
White-Box testing – este o tehnica de testare care examineaza
structura produsului si obtine date de testare
din codul programului
```
- statement coverage
- decision coverage

```
Experience-Based tests – este o tehnica de testare care se bazeaza pe
abilitatile si experienta anterioara a testerului cu
aceleasi tehnologii.
```
- error guessing test
- exploratory testing
- defect injection

• checklist based (^5)


Proiectul consta in testarea paginii web https://christiantour.ro pornind de la cerintele de

business formulate de către client (requirements). Ca sa putem verifica functionalitatea conform

cererii clientului, trebuie sa rulam teste ca sa putem identifica eventualele bug-uri.

Se defineste mediul de testare ca fiind - OS: Windows 10 Pro, Versiunea 21 H2 ;

- Browser: Google Chrome, Versiunea 106 .0 .5249.

```
Pasii de testare vor fi :
 Detalierea cerintelor de business (requirements)
 Scrierea conditiilor de testare (test condition)
 Scrierea test case-urilor si rularea lor
 Inregistrarea bug-urilor
 Analiza rezultatelor testelor
```
```
Pentru testare se vor folosi urmatoarele tool-uri :
 Jira – pentru adaugarea cerintelor de business si inregistrarea bug-urilor gasite in timpul testarii
 Test Rail – pentru creerea si rularea conditiilor de testare si a test case-urilor.
```

_Au fost primite din partea clientului un numar de 10 cerinte de business (requirements)_

_in vederea efectuarii de teste_


_Pentru cerintele de business (requirements) primite se detaliaza pasii care trebuie urmati._

_Se exemplifica mai jos doua din aceste cerinte._


_Dupa analizarea cerintelor de business (requirements) primite se stabilesc conditiile de testare_

_pentru fiecare requirement. Test case-urile se alcatuiesc pentru fiecare conditie de test stabilita._

_In continuare sunt prezentate conditiile de testare pentru cerinta de business CS- 1_


- Mai jos sunt prezentate conditiile de testare pentru cerinta de business CS-


_Am exemplificat mai jos un test case pentru cerinta de business CS-1 (test case-ul C76)_


_Mai jos inca un exemplu de test case pentru cerinta de business CS-5 (test case-ul C17)_


```
In urma testarilor de mai sus au fost identificate bug-uri. Dupa identificare sle au fost creeate
apoi in Jira dupa cum se poate observa mai jos.
```
_Bug-ul CS-12 asociat cerintei de business CS- 1 Bug-ul CS-11 asociat cerintei de business CS- 5_


### ID

Cerinte
Descriere cerinte

### ID

```
Test
Case
```
```
Denumire test Status
```
```
Defect
ID
```
### CS-

```
Ca utilizator doresc
sa-mi creez un cont
pe site-ul
http://www.chriatiantour.ro
```
```
C57 Se verifica functionalitatea butonului "Contul meu" Passed
```
```
C58 Se verifica functionalitatea butonului "Cont nou" din formularul de logare Passed
```
```
C59 Se verifica crearea contului dupa completarea tuturor campurilor si butoanelor din formularul de autentificare. Passed
```
```
C76 Se verifica crearea contului dupa completarea tuturor campurilor si butoanelor din formularul de autentificare cu un prenume invalid. Failed CS-
```
```
C79 Se verifica crearea contului dupa completarea tuturor campurilor si butoanelor din formularul de autentificare cu un nume invalid. Failed CS-
```
```
C80 Se formularulverifica crearea de autentificare contului dar dupa cu o completareaadresa de mail tuturorinvalida campurilor. si butoanelor din Passed
```
```
C93 Se formularulverifica crearea de autentificare contului cu o dupaparola completarea invalida tuturor campurilor si butoanelor din Passed
```
```
C122 Se autentificareverifica crearea fara a bifa contului butonul dupa cu " completareatermenii ș tuturori condițiile campurilor de utilizare din " formularul de Passed
```
```
C123 Se autentificareverifica crearea fara a bifa contului butonul dupa cu " completareaacordul pentru tuturor prelucrarea campurilor datelor din formularul" de Passed
```
```
C124 Se autentificareverifica crearea fara a bifa contului butonul dupa cu " completareaacordul pentru tuturor primirea campurilor de oferte din formularul" de Passed
```
### CS-

```
Ca utilizator doresc
sa pot schimba
moneda din EUR in
RON si invers
```
```
C
```
```
Verificarea existentei si functionalitatii butonului pentru schimbarea monedei din EUR
in RON si invers in pagina de baza a site-ului http://www.christiantour.ro Passed^
```
```
C17 Schimbarea monedei din EUR in RON si invers in sectiunea "SENIOR VOYAGE"^ Failed CS-
```
**MATRICEA DE TRASABILITATE**


**RAPOARTE DE TESTARE**

_Raport pentru cerinta de business CS- 5 Raport pentru cerinta de business CS- 1_

```
Tehnici de testare folosite :
-Boundary Value Analysis
-State Transition Testing
-Use Case Testing
```
**CONCLUZII :**

**In urma testarii paginii [http://www.christiantour.ro](http://www.christiantour.ro) au fost detectate unele defecte care pot avea impact asupra
functionalitatilor site-ului. Au fost efectuate 13 teste in cadrul acestui proiect din care 3 teste au au statusul
FA ILE D iar 10 teste au statusul PASSED.
Bug-urile au fost raportate spre remediere.
Avand in vedere ca bug-urile nu au un impact semnificativ asupra produsului finit nivelul de risc in cazul
site-ului web testat este unul moderat, site-ul poate fi folosit pana la rezolvarea defectelor, dar cu obligativitatea
ca acestea sa se rezolve odata cu updatarea noii versiuni a site-ului, intrucat numarul defectelor este mai mare
decat cel asteptat.**


